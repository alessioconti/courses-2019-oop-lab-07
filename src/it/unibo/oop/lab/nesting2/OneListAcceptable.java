package it.unibo.oop.lab.nesting2;

import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {

	List<T> list;

	public OneListAcceptable(List<T> inputList) {
		this.list = inputList;
	}

	/**
	 * Return the acceptor, i.e. the object which will take a sequence of elements
	 * in the very same order as defined on acceptable.
	 * 
	 * @return the acceptor
	 */
	public Acceptor<T> acceptor() {
		// TODO Auto-generated method stub
		return this.new AcceptorImpl<T>();
	}

	public class AcceptorImpl<A> implements Acceptor<T> {
		
		private int start;
		private int stop;
		
		public AcceptorImpl() {
			this.start = 0;
			this.stop = OneListAcceptable.this.list.size();
		}

		public void accept(T newElement) throws ElementNotAcceptedException {
			if (!OneListAcceptable.this.list.contains(newElement) && Integer.compare(this.start, this.stop) >= 0) {
				throw new ElementNotAcceptedException(newElement);
			}
			else this.start++;
		}

		public void end() throws EndNotAcceptedException {
			if (Integer.compare(this.start, this.stop) < 0) {
				throw new EndNotAcceptedException();
			}
		}
	}
}
