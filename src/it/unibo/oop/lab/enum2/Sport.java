/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {

    /*
     * TODO
     * 
     * Declare the following sports:
     * 
     * - basket
     * 
     * - volley
     * 
     * - tennis
     * 
     * - bike
     * 
     * - F1
     * 
     * - motogp
     * 
     * - soccer
     * 
     */
	BASKET(5, "Basket", Place.INDOOR),
	SOCCER(11, "Soccer", Place.OUTDOOR),
	TENNIS(2, "Tennis", Place.OUTDOOR),
	BIKE(1, "Bike", Place.OUTDOOR),
	F1(1, "F1", Place.OUTDOOR),
	MOTOGP(1, "MotoGP", Place.OUTDOOR),
	VOLLEY(6, "Volley", Place.INDOOR);

    /*
     * TODO
     * 
     * [FIELDS]
     * 
     * Declare required fields
     */
	private final int membersForTeam;
	private final String name;
	Place place;
	

    /*
     * TODO
     * 
     * [CONSTRUCTOR]
     * 
     * Define a constructor like this:
     * 
     * - Sport(final Place place, final int noTeamMembers, final String actualName)
     */
	private Sport(int membersForTeam, String name, Place place) {
		this.membersForTeam = membersForTeam;
		this.name = name;
		this.place = place;
	}

    /*
     * TODO
     * 
     * [METHODS] To be defined
     * 
     * 
     * 1) public boolean isIndividualSport()
     * 
     * Must return true only if called on individual sports
     */
	public boolean isIndividualSport() {
		return this.membersForTeam == 1;		
	}
	
     /* 
     * 2) public boolean isIndoorSport()
     * 
     * Must return true in case the sport is practices indoor
     */ 
	public boolean isIndoorSport() {
		return this.place == Place.INDOOR;
	}
	
	/* 
     * 3) public Place getPlace()
     * 
     * Must return the place where this sport is practiced
     */ 
	public Place getPlace() {
		return this.place;
	}
	
	/* 
     * 4) public String toString()
     * 
     * Returns the string representation of a sport
     */
	public String toString() {
		return "Sport: " + this.name + "team composed by: " + this.membersForTeam + " members " + "practiced: " + this.place;
	}
}
